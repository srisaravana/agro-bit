<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Farmer;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "first_name" => Request::getAsString("first_name", true),
        "last_name" => Request::getAsString("last_name", true),
        "nic" => Request::getAsString("nic", true),
        "contact_number" => Request::getAsString("contact_number", true),
        "address" => Request::getAsString("address", true),
        "gs_division" => Request::getAsString("gs_division", true),
    ];


    $farmer = Farmer::build($fields);

    $result = $farmer->insert();

    if ($result) {

        $farmer = Farmer::find($result);

        JSONResponse::validResponse(["farmer" => $farmer]);
        return;
    }


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
