<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Farmer;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $id = Request::getAsInteger("id", true);

    if (empty($id)) throw new Exception("Invalid id");

    $farmer = Farmer::find($id);

    if (is_null($farmer)) throw new Exception("Invalid farmer");

    JSONResponse::validResponse(["farmer" => $farmer]);
    return;


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
