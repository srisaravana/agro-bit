<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Fertilizers\Fertilizer;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "id" => Request::getAsInteger( "id", true ),
        "fert_name" => Request::getAsString( "fert_name", true ),
        "code" => Request::getAsString( "code", true ),
        "market_price" => Request::getAsFloat( "market_price", true ),
    ];

    if ( empty( Fertilizer::find( $fields["id"] ) ) ) throw new Exception( "Invalid fertilizer" );

    $fert = Fertilizer::build( $fields );

    $result = $fert->update();

    if ( $result ) {
        JSONResponse::validResponse( "Updated" );
        return;
    }
    throw new Exception( "Failed to update" );


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
