<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Fertilizers\Fertilizer;
use App\Models\Fertilizers\FertilizerInvoice;

require_once "../../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "fert_id" => Request::getAsInteger( "fert_id", true ),
        "invoice_number" => Request::getAsString( "invoice_number", true ),
        "date_of_invoice" => Request::getAsString( "date_of_invoice", true ),
        "description" => Request::getAsString( "description", true ),
        "number_of_bags" => Request::getAsInteger( "number_of_bags", true ),
        "total_weight" => Request::getAsFloat( "total_weight", true ),
        "unit_price" => Request::getAsFloat( "unit_price", true ),
        "amount" => Request::getAsFloat( "amount", true ),
        "discount" => Request::getAsFloat( "discount", true ),
        "net_amount" => Request::getAsFloat( "net_amount", true ),
    ];


    if ( is_null( $fields["fert_id"] ) || is_null( $fields["invoice_number"] ) ) throw new Exception( "Required fields missing" );

    if ( empty( Fertilizer::find( $fields["fert_id"] ) ) ) throw new Exception( "Invalid fertilizer" );

    $fertInvoice = FertilizerInvoice::build( $fields );

    $result = $fertInvoice->insert();

    if ( $result ) {

        $fert = FertilizerInvoice::find( $result );

        JSONResponse::validResponse( [ "invoice" => $fert ] );
        return;
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
