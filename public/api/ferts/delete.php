<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Fertilizers\Fertilizer;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "id" => Request::getAsInteger("id", true),
    ];

    $fert = Fertilizer::find($fields["id"]);

    if (empty($fert)) throw new Exception("Invalid id");


    $fert = Fertilizer::build($fields);
    $result = $fert->delete();

    if ($result) {
        JSONResponse::validResponse("Deleted");
        return;
    }
    throw new Exception("Failed to delete");


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
