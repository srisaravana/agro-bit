<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Fertilizers\Fertilizer;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "fert_name" => Request::getAsString( "fert_name", true ),
        "code" => Request::getAsString( "code", true ),
        "market_price" => Request::getAsFloat( "market_price", true ),
    ];


    if ( is_null( $fields["fert_name"] ) || is_null( $fields["code"] ) ) throw new Exception( "Required fields missing" );


    $fert = Fertilizer::build( $fields );

    $result = $fert->insert();

    if ( $result ) {

        $fert = Fertilizer::find( $result );

        JSONResponse::validResponse( [ "fertilizer" => $fert ] );
        return;
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
