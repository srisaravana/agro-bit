<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Land;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "id" => Request::getAsInteger("id", true),
        "reg_number" => Request::getAsString("reg_number", true),
        "continent_name" => Request::getAsString("continent_name", true),
        "land_size" => Request::getAsFloat("land_size", true),
        "lat" => Request::getAsFloat("lat"),
        "lng" => Request::getAsFloat("lng"),
    ];


    if (empty($fields["reg_number"])) throw new Exception("Registration number required");

    $land = Land::build($fields);

    $result = $land->update();

    if ($result) {
        JSONResponse::validResponse("Updated");
        return;
    }
    throw new Exception("Failed to update");


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
