<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Farmer;
use App\Models\Land;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "farmer_id" => Request::getAsInteger("farmer_id", true),
        "reg_number" => Request::getAsString("reg_number", true),
        "continent_name" => Request::getAsString("continent_name", true),
        "land_size" => Request::getAsFloat("land_size", true),
        "lat" => Request::getAsFloat("lat"),
        "lng" => Request::getAsFloat("lng"),
    ];

    /*
     * we are testing if there is a valid farmer exist for the given
     * farmer_id, otherwise throw an exception
     * */

    if (is_null($fields["farmer_id"])) throw new Exception("Invalid farmer");

    $farmer = Farmer::find($fields["farmer_id"]);
    if (empty($farmer)) throw new Exception("Invalid farmer");


    $land = Land::build($fields);

    $result = $land->insert();

    if ($result) {

        $land = Land::find($result);

        JSONResponse::validResponse(["land" => $land]);
        return;
    }


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
