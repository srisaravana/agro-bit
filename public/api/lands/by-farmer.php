<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Farmer;
use App\Models\Land;

require_once "../../../bootstrap.php";


try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();

    $id = Request::getAsInteger("id", true);

    $farmer = Farmer::find($id);

    if (!empty($farmer)) {
        $lands = Land::byFarmer($farmer);
        JSONResponse::validResponse($lands);
        return;
    }

    throw new Exception("Failed");


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}

