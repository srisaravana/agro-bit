<?php


namespace App\Models\Fertilizers;


use App\Core\Database\Database;
use App\Models\IModel;
use stdClass;

class Fertilizer implements IModel
{

    public const TABLE = "ferts";

    public ?int $id;
    public ?string $fert_name, $code;
    public ?float $market_price;

    /* calculated values */
    public ?int $total_weight;


    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    /**
     * @param int $id
     * @return self|null
     */
    public static function find( int $id ): ?Fertilizer
    {
        return Database::find( self::TABLE, $id, self::class );
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return self[]
     */
    public static function findAll( $limit = 1000, $offset = 0 ): array
    {
        /** @var self[] $results */
        $results = Database::findAll( self::TABLE, $limit, $offset, self::class, "code" );

        if ( !empty( $results ) ) {
            foreach ( $results as $result ) {
                $result->calculateTotalWeight();
            }

            return $results;
        }

        return [];
    }

    /**
     * @return int
     */
    public function insert(): int
    {
        $data = [
            "fert_name" => $this->fert_name,
            "code" => $this->code,
            "market_price" => $this->market_price,
        ];


        return Database::insert( self::TABLE, $data );
    }

    public function update(): bool
    {
        $data = [
            "fert_name" => $this->fert_name,
            "code" => $this->code,
            "market_price" => $this->market_price,
        ];

        return Database::update( self::TABLE, $data, [ "id" => $this->id ] );
    }

    public function delete(): bool
    {
        return Database::delete( self::TABLE, "id", $this->id );
    }

    public function calculateTotalWeight()
    {
        $db = Database::instance();
        $statement = $db->prepare( "SELECT SUM(total_weight) AS result FROM fert_invoices WHERE fert_id=?" );
        $statement->execute( [ $this->id ] );

        $result = $statement->fetchObject( stdClass::class );

        if ( !empty( $result ) ) {
            $this->total_weight = (int)$result->result;
        } else {
            $this->total_weight = 0;
        }
    }

}
