<?php


namespace App\Models\Fertilizers;


use App\Core\Database\Database;
use App\Models\IModel;
use PDO;

class FertilizerInvoice implements IModel
{

    public const TABLE = "fert_invoices";

    public ?int $id, $fert_id, $number_of_bags;
    public ?string $date_of_invoice, $invoice_number, $description;
    public ?float $total_weight, $unit_price, $amount, $discount, $net_amount;

    public ?Fertilizer $fertilizer;

    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    public static function find( int $id ): ?FertilizerInvoice
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->fertilizer = Fertilizer::find( $result->fert_id );
            return $result;
        }
        return null;
    }


    public static function findAll( $limit = 1000, $offset = 0 ): array
    {
        return Database::findAll( self::TABLE, $limit, $offset, self::class, "invoice_number" );
    }


    public function insert(): int
    {
        $data = [
            "fert_id" => $this->fert_id,
            "date_of_invoice" => $this->date_of_invoice,
            "invoice_number" => $this->invoice_number,
            "description" => $this->description,
            "number_of_bags" => $this->number_of_bags,
            "total_weight" => $this->total_weight,
            "unit_price" => $this->unit_price,
            "amount" => $this->amount,
            "discount" => $this->discount,
            "net_amount" => $this->net_amount,
        ];

        return Database::insert( self::TABLE, $data );
    }

    public function update(): bool
    {
        $data = [
            "date_of_invoice" => $this->date_of_invoice,
            "invoice_number" => $this->invoice_number,
            "description" => $this->description,
            "number_of_bags" => $this->number_of_bags,
            "total_weight" => $this->total_weight,
            "unit_price" => $this->unit_price,
            "amount" => $this->amount,
            "discount" => $this->discount,
            "net_amount" => $this->net_amount,
        ];

        return Database::update( self::TABLE, $data, [ "id" => $this->id ] );
    }

    public function delete(): bool
    {
        return Database::delete( self::TABLE, "id", $this->id );
    }


    /**
     * @param Fertilizer $fertilizer
     * @return FertilizerInvoice[]
     */
    public static function findByFertilizer( Fertilizer $fertilizer ): array
    {

        $db = Database::instance();
        $statement = $db->prepare( "select * from fert_invoices where fert_id = ?" );
        $statement->execute( [ $fertilizer->id ] );

        /** @var self[] $results */
        $results = $statement->fetchAll( PDO::FETCH_CLASS, self::class );

        if ( !empty( $results ) ) return $results;
        return [];

    }


}
