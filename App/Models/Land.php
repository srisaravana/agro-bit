<?php


namespace App\Models;


use App\Core\Database\Database;
use PDO;

class Land implements IModel
{

    private const TABLE = "lands";

    public ?int $id, $farmer_id;
    public ?float $land_size, $lat, $lng;
    public ?string $reg_number, $continent_name;

    public ?Farmer $farmer;

    public static function build($array): Land
    {
        $object = new self();
        foreach ($array as $key => $value) {
            $object->$key = $value;
        }
        return $object;
    }

    public static function find(int $id): ?Land
    {
        /** @var Land $result */
        $result = Database::find(self::TABLE, $id, self::class);

        if (!empty($result)) {
            $result->farmer = Farmer::find($result->farmer_id);

            return $result;
        }

        return null;

    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Land[]
     */
    public static function findAll($limit = 1000, $offset = 0): array
    {
        /** @var Land[] $results */
        $results = Database::findAll(self::TABLE, $limit, $offset, self::class, "farmer_id");

        if (!empty($results)) {

            $output = [];

            foreach ($results as $result) {
                $result->farmer = Farmer::find($result->farmer_id);
                $output[] = $result;
            }

            return $output;

        }

        return [];
    }

    /**
     * @return bool|int
     */
    public function insert()
    {
        $data = [
            "reg_number" => $this->reg_number,
            "continent_name" => $this->continent_name,
            "land_size" => $this->land_size,
            "lat" => $this->lat,
            "lng" => $this->lng,
            "farmer_id" => $this->farmer_id,
        ];

        return Database::insert(self::TABLE, $data);

    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        $data = [
            "reg_number" => $this->reg_number,
            "continent_name" => $this->continent_name,
            "land_size" => $this->land_size,
            "lat" => $this->lat,
            "lng" => $this->lng,
        ];

        return Database::update(self::TABLE, $data, ["id" => $this->id]);

    }

    public function delete(): bool
    {
        return Database::delete(self::TABLE, "id", $this->id);
    }


    public static function byFarmer(Farmer $farmer): array
    {

        $db = Database::instance();
        $statement = $db->prepare("select * from lands where farmer_id=?");
        $statement->execute([$farmer->id]);

        /** @var Land[] $results */
        $results = $statement->fetchAll(PDO::FETCH_CLASS, self::class);

        if (!empty($results)) return $results;
        return [];

    }

}
