<?php

declare(strict_types=1);

namespace App\Models;


use App\Core\Database\Database;

class Farmer implements IModel
{

    public const TABLE = "farmers";

    public ?int $id;
    public ?string $first_name, $last_name, $nic, $contact_number, $address, $gs_division;

    public static function build($array): self
    {
        $object = new self();
        foreach ($array as $key => $value) {
            $object->$key = $value;
        }
        return $object;
    }


    /**
     * @param int $id
     * @return Farmer|null
     */
    public static function find(int $id): ?Farmer
    {
        return Database::find(self::TABLE, $id, self::class);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Farmer[]|null
     */
    public static function findAll($limit = 1000, $offset = 0): ?array
    {
        return Database::findAll(self::TABLE, $limit, $offset, self::class, "first_name");
    }

    /**
     * @return bool|int
     */
    public function insert()
    {
        $data = [
            "first_name" => $this->first_name,
            "last_name" => $this->last_name,
            "nic" => $this->nic,
            "contact_number" => $this->contact_number,
            "address" => $this->address,
            "gs_division" => $this->gs_division,
        ];

        return Database::insert(self::TABLE, $data);

    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        $data = [
            "first_name" => $this->first_name,
            "last_name" => $this->last_name,
            "nic" => $this->nic,
            "contact_number" => $this->contact_number,
            "address" => $this->address,
            "gs_division" => $this->gs_division,
        ];

        return Database::update(self::TABLE, $data, ["id" => $this->id]);

    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        return Database::delete(self::TABLE, "id", $this->id);
    }
}
