-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.13-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6333
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for agro
DROP DATABASE IF EXISTS `agro`;
CREATE DATABASE IF NOT EXISTS `agro` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `agro`;

-- Dumping structure for table agro.farmers
DROP TABLE IF EXISTS `farmers`;
CREATE TABLE IF NOT EXISTS `farmers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `nic` varchar(50) NOT NULL,
  `contact_number` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table agro.farmers: ~4 rows (approximately)
DELETE FROM `farmers`;
/*!40000 ALTER TABLE `farmers` DISABLE KEYS */;
INSERT INTO `farmers` (`id`, `first_name`, `last_name`, `nic`, `contact_number`, `address`) VALUES
	(1, 'Blueberry', 'Avocado', '0123456789V', '0645125471', '#234, \nMain road, Vaharai'),
	(2, 'Mango', 'Banana', '9874562122V', '07741512356', 'Main road, Vaharai'),
	(3, 'Plump', 'Pineapple', '9898941761V', '0779845621', '#45/3, Main Street,\nVaharai'),
	(4, 'Apple', 'Mango', '9184519198V', '0651234121', '#89/23,\nKallady');
/*!40000 ALTER TABLE `farmers` ENABLE KEYS */;

-- Dumping structure for table agro.ferts
DROP TABLE IF EXISTS `ferts`;
CREATE TABLE IF NOT EXISTS `ferts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fert_name` varchar(255) NOT NULL,
  `code` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table agro.ferts: ~2 rows (approximately)
DELETE FROM `ferts`;
/*!40000 ALTER TABLE `ferts` DISABLE KEYS */;
INSERT INTO `ferts` (`id`, `fert_name`, `code`) VALUES
	(10, 'MOB', 'S003'),
	(11, 'Urea', 'S321'),
	(12, 'New fert', 'S111');
/*!40000 ALTER TABLE `ferts` ENABLE KEYS */;

-- Dumping structure for table agro.fert_invoices
DROP TABLE IF EXISTS `fert_invoices`;
CREATE TABLE IF NOT EXISTS `fert_invoices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_of_invoice` date NOT NULL,
  `invoice_number` varchar(50) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `number_of_bags` tinyint(4) NOT NULL DEFAULT 0,
  `total_weight` float NOT NULL DEFAULT 0,
  `unit_price` float NOT NULL DEFAULT 0,
  `amount` float NOT NULL DEFAULT 0,
  `discount` float NOT NULL DEFAULT 0,
  `net_amount` float NOT NULL DEFAULT 0,
  `fert_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fert_invoices_ferts` (`fert_id`),
  CONSTRAINT `FK_fert_invoices_ferts` FOREIGN KEY (`fert_id`) REFERENCES `ferts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table agro.fert_invoices: ~3 rows (approximately)
DELETE FROM `fert_invoices`;
/*!40000 ALTER TABLE `fert_invoices` DISABLE KEYS */;
INSERT INTO `fert_invoices` (`id`, `date_of_invoice`, `invoice_number`, `description`, `number_of_bags`, `total_weight`, `unit_price`, `amount`, `discount`, `net_amount`, `fert_id`) VALUES
	(1, '2012-06-04', 'INV-001', 'some more uria needed', 20, 2000, 1500, 30000, 2000, 28000, 10),
	(2, '2021-06-24', '1212', 'asdasdasd', 45, 2700, 1500, 67500, 3200, 64300, 10),
	(3, '2021-06-24', '12312', 'This is a sample descripiton', 12, 900, 990, 11880, 1350.5, 10529.5, 11),
	(4, '2021-06-24', '74512', NULL, 45, 1350, 1100, 49500, 2400, 47100, 11),
	(5, '2021-06-26', '13123', NULL, 34, 2040, 450, 15300, 1200, 14100, 10);
/*!40000 ALTER TABLE `fert_invoices` ENABLE KEYS */;

-- Dumping structure for table agro.lands
DROP TABLE IF EXISTS `lands`;
CREATE TABLE IF NOT EXISTS `lands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reg_number` varchar(50) NOT NULL,
  `continent_name` varchar(255) NOT NULL,
  `land_size` float NOT NULL DEFAULT 0,
  `lat` float DEFAULT 0,
  `lng` float DEFAULT 0,
  `farmer_id` int(10) unsigned NOT NULL DEFAULT 0,
  `type` enum('OWNED','LEASED') DEFAULT 'OWNED',
  PRIMARY KEY (`id`),
  KEY `FK_lands_farmers` (`farmer_id`),
  CONSTRAINT `FK_lands_farmers` FOREIGN KEY (`farmer_id`) REFERENCES `farmers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table agro.lands: ~4 rows (approximately)
DELETE FROM `lands`;
/*!40000 ALTER TABLE `lands` DISABLE KEYS */;
INSERT INTO `lands` (`id`, `reg_number`, `continent_name`, `land_size`, `lat`, `lng`, `farmer_id`, `type`) VALUES
	(1, '67/EE/67561', 'Valaichenai', 5.24, 78.2263, 95.2125, 2, 'OWNED'),
	(2, '34/211/ABS', 'Vakarai', 4.5, NULL, NULL, 2, 'LEASED'),
	(3, '34/211/ABS', 'Vakarai', 4.5, NULL, NULL, 1, 'OWNED'),
	(4, '112233/ABD', 'Valaichenai', 1.85, 123123, 12312300, 3, 'OWNED'),
	(12, '56-32JLK', 'Valaicnenai', 5.6, NULL, NULL, 3, 'LEASED');
/*!40000 ALTER TABLE `lands` ENABLE KEYS */;

-- Dumping structure for table agro.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password_hash` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `role` enum('ADMIN','MANAGER','USER') NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table agro.users: ~0 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password_hash`, `email`, `full_name`, `role`) VALUES
	(1, 'admin', '$2y$10$JN/JQbRZ8zj6ReU5StNgc.AXIWuw7c8OexEk1Hlnh7/TBkuDzdyp2', 'admin@hello.com', 'Administrator', 'ADMIN'),
	(2, 'Mango', '$2y$10$hX5a2Edv8mrko7xXGOSWoOjiw3BOSHtcPF3IKosQBbmD8mf7ae7li', 'mango@gmail.com', 'Mango', 'USER');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table agro.users_auth_keys
DROP TABLE IF EXISTS `users_auth_keys`;
CREATE TABLE IF NOT EXISTS `users_auth_keys` (
  `user_id` int(11) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `valid_till` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`user_id`),
  CONSTRAINT `FK_users_auth_keys_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table agro.users_auth_keys: ~1 rows (approximately)
DELETE FROM `users_auth_keys`;
/*!40000 ALTER TABLE `users_auth_keys` DISABLE KEYS */;
INSERT INTO `users_auth_keys` (`user_id`, `auth_key`, `valid_till`) VALUES
	(1, '5f754fb8625c217002ae459d83a0d4bf', '2021-07-22 00:35:22');
/*!40000 ALTER TABLE `users_auth_keys` ENABLE KEYS */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
