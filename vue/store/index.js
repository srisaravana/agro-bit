import Vue from "vue";
import Vuex from "vuex";

import Auth from "./modules/auth";
import Users from "./modules/users";
import Farmers from "./modules/farmers";
import {lands} from "./modules/lands";
import {fertilizer} from "./modules/fertilizers";
import {fertInvoices} from "./modules/fert_invoices";

Vue.use(Vuex);

export default new Vuex.Store({

    state: {},
    mutations: {},
    actions: {},
    modules: {
        auth: Auth,
        users: Users,
        farmers: Farmers,
        lands: lands,
        fertilizers: fertilizer,
        fertInvoices: fertInvoices,
    }

});
