import axios from "axios";

export default {

    state: {

        farmers: [],
        farmer: {},

    },

    getters: {

        getFarmers(state) { return state.farmers; },
        getFarmer(state) { return state.farmer; },

    },

    mutations: {
        setFarmers(state, data) { state.farmers = data; },
        setFarmer(state, data) { state.farmer = data; },
    },

    actions: {

        async farmers_fetchAll(context) {

            try {

                const response = await axios.get("farmers/all.php");
                context.commit("setFarmers", response.data.payload);

            } catch (e) {
                throw e;
            }

        },

        async farmers_fetch(context, id) {
            try {
                const response = await axios.get(`farmers/get.php?id=${id}`);
                context.commit("setFarmer", response.data.payload.farmer);

            } catch (e) {
                throw e;
            }
        },

        async farmers_create(context, params) {

            try {
                await axios.post("farmers/create.php", params);
            } catch (e) {
                throw e;
            }
        },

        async farmers_update(context, params) {
            try {

                await axios.post("farmers/update.php", params);

            } catch (e) {
                throw e;
            }
        }

    },

};
