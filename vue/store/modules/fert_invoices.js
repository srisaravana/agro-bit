import axios from "axios";

export const fertInvoices = {
    state() {
        return {

            fertInvoiceList: [],
            selectedFertInvoice: {},

        };
    },

    getters: {

        getFertInvoiceList( state ) {
            return state.fertInvoiceList;
        },

        getSelectedFertInvoice( state ) {
            return state.selectedFertInvoice
        }

    },

    actions: {

        async fertInvoices_fetchAll( context, fertId ) {
            try {
                let response = await axios.get( `ferts/invoice/by-fertilizer.php?id=${ fertId }` );
                context.state.fertInvoiceList = response.data.payload;

            } catch ( e ) {
                throw e;
            }
        }, /* fetch all */

        async fertInvoices_create( context, params ) {
            try {
                await axios.post( "ferts/invoice/create.php", params );
            } catch ( e ) {
                throw e;
            }
        }, /* create */

    },
    /* -- actions -- */
};
