import axios from "axios";

export const lands = {
    state: {

        land: {},
        lands: [],

    },

    getters: {

        getLands(state) { return state.lands; },
        getLand(state) {return state.land; },

    },

    mutations: {

        setLands(state, data) { state.lands = data; },
        setLand(state, data) { state.land = data;},

    },

    actions: {


        async lands_fetch(context, id) {
            try {

                const response = await axios.get(`lands/get.php?id=${id}`);
                context.commit("setLand", response.data.payload.land);

            } catch (e) {
                throw e;
            }
        },

        async lands_fetchByFarmer(context, id) {

            try {

                const response = await axios.get(`lands/by-farmer.php?id=${id}`);
                context.commit("setLands", response.data.payload);

            } catch (e) {
                throw e;
            }

        }, /* fetch by farmer */

        async lands_create(context, params) {

            try {
                await axios.post("lands/create.php", params);
            } catch (e) {
                throw e;
            }

        }, /* create */

        async lands_update(context, params) {

            try {
                await axios.post("lands/update.php", params);
            } catch (e) {
                throw e;
            }

        }, /* update */

    },
}
