import axios from "axios";

export const fertilizer = {

    state: {

        fertilizers: null,
        fertilizer: null,

    },

    getters: {

        getFertilizers( state ) {
            return state.fertilizers;
        },

        getFertilizer( state ) {
            return state.fertilizer;
        },

    },

    mutations: {
        setFertilizers( state, data ) {
            state.fertilizers = data;
        },

        setFertilizer( state, data ) {
            state.fertilizer = data;
        },
    },

    actions: {

        async fertilizers_fetchAll( context ) {

            try {

                const response = await axios.get( "ferts/all.php" );
                context.commit( "setFertilizers", response.data.payload );

            } catch ( e ) {
                throw e;
            }

        },

        async fertilizers_fetch( context, id ) {
            try {
                const response = await axios.get( `ferts/get.php?id=${ id }` );
                context.commit( "setFertilizer", response.data.payload.fertilizer );

            } catch ( e ) {
                throw e;
            }
        },

        async fertilizers_create( context, params ) {

            try {
                await axios.post( "ferts/create.php", params );
            } catch ( e ) {
                throw e;
            }
        },

        async fertilizers_update( context, params ) {
            try {

                await axios.post( "ferts/update.php", params );

            } catch ( e ) {
                throw e;
            }
        },

        async fertilizers_delete( context, params ) {
            try {

                await axios.post( "ferts/delete.php", params );

            } catch ( e ) {
                throw e;
            }
        }

    },

};
