import ManageFertilizers from "../../views/fertilizers/ManageFertilizers";
import EditFertilizer from "../../views/fertilizers/EditFertilizer";
import AddFertInvoice from "../../views/fertilizers/AddFertInvoice";

export const fertilizerRoutes = [
    {
        path: "/fertilizers",
        name: "ManageFertilizers",
        component: ManageFertilizers,
        meta: {
            requiresAuth: true,
            adminOnly: true,
        }
    },
    {
        path: "/fertilizers/edit/:id",
        name: "EditFertilizers",
        component: EditFertilizer,
        meta: {
            requiresAuth: true,
            adminOnly: true,
        }
    },

    {
        path: "/fertilizers/edit/:id/add-invoice",
        name: "AddFertInvoice",
        component: AddFertInvoice,
        meta: {
            requiresAuth: true,
            adminOnly: true,
        }
    },


];
