import ManageFarmers from "../../views/farmers/ManageFarmers";
import CreateFarmer from "../../views/farmers/CreateFarmer";
import EditFarmer from "../../views/farmers/EditFarmer";
import EditLand from "../../views/farmers/lands/EditLand";

export const farmerRoutes = [
    {
        path: "/farmers",
        name: "ManageFarmers",
        component: ManageFarmers,
        meta: {
            requiresAuth: true,
            adminOnly: true,
        }
    },
    {
        path: "/farmers/add",
        name: "AddFarmers",
        component: CreateFarmer,
        meta: {
            requiresAuth: true,
            adminOnly: true,
        }
    },
    {
        path: "/farmers/edit/:id",
        name: "EditFarmer",
        component: EditFarmer,
        meta: {
            requiresAuth: true,
            adminOnly: true,
        }
    },

    {
        path: "/farmers/:farmerId/lands/edit/:id",
        name: "EditLand",
        component: EditLand,
        meta: {
            requiresAuth: true,
            adminOnly: true,
        }
    },


];
